# SpringIoc

How to use Spring IoC

**Example 1**

In this example:

*  all beans are configured in `class Config`
*  in `class Main` we use `javax.inject.Inject annotation` on constructor to inject dependency
*  in `class ServiceMessageImpl`  we use `javax.inject.Inject` annotation  on constructor to inject dependency

**Example 2**

In this example:

*  all beans are configured in `class Config`
*  in `class Main` we use `javax.inject.Inject` annotation on constructor to inject dependency
*  in `class ServiceMessageImpl` we use `javax.inject.Inject` annotation  on set method to inject dependency


**Example 3**

In this example:

*  some beans are configured in `class Config` `@Bean(name = "txt")` and` @Bean(name = "xml")` , while `@Bean(name = "serviceMessage")` and ` @Bean(name = "main")` are configured using annotation `@Component` and `@ComponentScan` and will be created automatically 
*  in `class Main ` we use `javax.inject.Inject` annotation on constructor to inject dependency
*  in `class ServiceMessageImpl` we use `javax.inject.Inject` annotation on set method to inject dependency


**Example 4**

In this example:

*  all beans are configured using annotation `@Component` and `@ComponentScan `and will be created automatically 
*  in `class Main` we use `org.springframework.beans.factory.annotation.Autowired`  annotation  to inject dependency
*  in `class ServiceMessageImpl `we use `org.springframework.beans.factory.annotation.Autowired` annotation to inject dependency