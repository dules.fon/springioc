package rs.ac.bg.fon.silab.spring.ioc.example3.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import rs.ac.bg.fon.silab.spring.ioc.example3.repository.Repository;
import rs.ac.bg.fon.silab.spring.ioc.example3.repository.impl.TxtRespository;
import rs.ac.bg.fon.silab.spring.ioc.example3.repository.impl.XmlRepository;

/**
 *
 * @author Dusan
 */

@Configuration
@ComponentScan(basePackages = {
    "rs.ac.bg.fon.silab.spring.ioc.example3.service",
    "rs.ac.bg.fon.silab.spring.ioc.example3.main"})
public class Config {

    @Bean(name = "txt")
    public Repository txtRepository() {
        return new TxtRespository();
    }

    @Bean(name = "xml")
    public Repository xmlRepository() {
        return new XmlRepository();
    }


}
