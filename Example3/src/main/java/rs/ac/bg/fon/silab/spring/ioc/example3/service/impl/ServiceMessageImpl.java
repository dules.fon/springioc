package rs.ac.bg.fon.silab.spring.ioc.example3.service.impl;

import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.stereotype.Component;
import rs.ac.bg.fon.silab.spring.ioc.example3.repository.Repository;
import rs.ac.bg.fon.silab.spring.ioc.example3.service.ServiceMessage;

/**
 *
 * @author Dusan
 */
@Component(value = "serviceMessage")
public class ServiceMessageImpl implements ServiceMessage {
    private Repository repository;

    @Inject
    @Named(value = "xml")
    public void setRepository(Repository repository) {
        this.repository = repository;
    }


    @Override
    public void save(String message) {
        repository.save(message);
    }

}
