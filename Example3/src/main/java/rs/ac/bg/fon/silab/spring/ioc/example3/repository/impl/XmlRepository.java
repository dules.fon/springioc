package rs.ac.bg.fon.silab.spring.ioc.example3.repository.impl;

import org.springframework.stereotype.Component;
import rs.ac.bg.fon.silab.spring.ioc.example3.repository.Repository;

/**
 *
 * @author Dusan
 */

@Component(value = "xml")
public class XmlRepository implements Repository{

    @Override
    public void save(String message) {
        System.out.println("<xml>"+message+"</xml>");
    }
    
}
