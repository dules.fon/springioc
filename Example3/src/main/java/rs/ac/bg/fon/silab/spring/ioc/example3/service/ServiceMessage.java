package rs.ac.bg.fon.silab.spring.ioc.example3.service;

/**
 *
 * @author Dusan
 */
public interface ServiceMessage {
    
    void save(String message);
    
}
