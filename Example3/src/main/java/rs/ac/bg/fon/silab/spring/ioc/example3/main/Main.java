package rs.ac.bg.fon.silab.spring.ioc.example3.main;

import javax.inject.Inject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import rs.ac.bg.fon.silab.spring.ioc.example3.config.Config;
import rs.ac.bg.fon.silab.spring.ioc.example3.service.ServiceMessage;

/**
 *
 * @author Dusan
 */
@Component
public class Main {

    private final ServiceMessage serviceMessage;

    @Inject
    public Main(ServiceMessage serviceMessage) {
        this.serviceMessage = serviceMessage;
    }

    private void saveMessage() {
        System.out.println("Saving message...");
        serviceMessage.save("My message that I want to save.");
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(Config.class);

        Main main = applicationContext.getBean(Main.class);
        main.saveMessage();
    }

}
