package rs.ac.bg.fon.silab.spring.ioc.example2.repository.impl;
import org.springframework.stereotype.Component;
import rs.ac.bg.fon.silab.spring.ioc.example2.repository.Repository;

/**
 *
 * @author Dusan
 */

@Component(value = "txt")
public class TxtRespository implements Repository{

    @Override
    public void save(String message) {
        System.out.println(message);
    }
    
}
