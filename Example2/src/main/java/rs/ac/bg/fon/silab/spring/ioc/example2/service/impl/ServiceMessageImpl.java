package rs.ac.bg.fon.silab.spring.ioc.example2.service.impl;

import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.spring.ioc.example2.repository.Repository;
import rs.ac.bg.fon.silab.spring.ioc.example2.service.ServiceMessage;

/**
 *
 * @author Dusan
 */
@Service(value = "serviceMessage")
public class ServiceMessageImpl implements ServiceMessage {
    private  Repository repository;
    
    @Inject
    @Named(value = "xml")
    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void save(String message) {
        repository.save(message);
    }

}
