package rs.ac.bg.fon.silab.spring.ioc.example2.service;

/**
 *
 * @author Dusan
 */
public interface ServiceMessage {
    
    void save(String message);
    
}
