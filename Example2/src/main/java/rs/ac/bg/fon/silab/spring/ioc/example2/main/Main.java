package rs.ac.bg.fon.silab.spring.ioc.example2.main;

import javax.inject.Inject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import rs.ac.bg.fon.silab.spring.ioc.example2.config.Config;
import rs.ac.bg.fon.silab.spring.ioc.example2.service.ServiceMessage;

/**
 *
 * @author Dusan
 */
public class Main {

    private final ServiceMessage serviceMessage;
    
    @Inject
    public Main(ServiceMessage serviceMessage) {
        this.serviceMessage = serviceMessage;
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(Config.class);

        Main main = applicationContext.getBean(Main.class);
        main.saveMessage();
    }

    private void saveMessage() {
        System.out.println("Saving message...");
        serviceMessage.save("My message that I want to save.");
    }
}
