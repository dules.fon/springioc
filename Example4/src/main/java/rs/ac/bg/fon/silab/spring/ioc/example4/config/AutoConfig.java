package rs.ac.bg.fon.silab.spring.ioc.example4.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Dusan
 */
@Configuration
@ComponentScan(basePackages = {
    "rs.ac.bg.fon.silab.spring.ioc.example4.repository",
    "rs.ac.bg.fon.silab.spring.ioc.example4.service",
    "rs.ac.bg.fon.silab.spring.ioc.example4.main"})
public class AutoConfig {
    
}
