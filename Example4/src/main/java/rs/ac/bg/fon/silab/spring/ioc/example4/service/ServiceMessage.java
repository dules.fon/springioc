package rs.ac.bg.fon.silab.spring.ioc.example4.service;

/**
 *
 * @author Dusan
 */
public interface ServiceMessage {
    
    void save(String message);
    
}
