package rs.ac.bg.fon.silab.spring.ioc.example4.repository.impl;
import org.springframework.stereotype.Component;
import rs.ac.bg.fon.silab.spring.ioc.example4.repository.Repository;

/**
 *
 * @author Dusan
 */

@org.springframework.stereotype.Repository(value = "txt")
public class TxtRespository implements Repository{

    @Override
    public void save(String message) {
        System.out.println(message);
    }
    
}
