/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.bg.fon.silab.spring.ioc.example4.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import rs.ac.bg.fon.silab.spring.ioc.example4.config.AutoConfig;
import rs.ac.bg.fon.silab.spring.ioc.example4.service.ServiceMessage;

/**
 * 
 * @author Dusan
 */
@Component
public class Main {

    @Autowired
    private ServiceMessage serviceMessage;

    private void saveMessage() {
        System.out.println("Saving message...");
        serviceMessage.save("My message that I want to save.");
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext
                = new AnnotationConfigApplicationContext(AutoConfig.class);

        Main main = applicationContext.getBean(Main.class);
        main.saveMessage();
    }

}
