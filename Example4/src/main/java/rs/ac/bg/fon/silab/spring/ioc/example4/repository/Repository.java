package rs.ac.bg.fon.silab.spring.ioc.example4.repository;

/**
 *
 * @author Dusan
 */
public interface Repository {
    void save(String message);
}
