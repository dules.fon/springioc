package rs.ac.bg.fon.silab.spring.ioc.example4.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.spring.ioc.example4.repository.Repository;
import rs.ac.bg.fon.silab.spring.ioc.example4.service.ServiceMessage;

/**
 *
 * @author Dusan
 */
@Service(value = "serviceMessage")
public class ServiceMessageImpl implements ServiceMessage {

    @Autowired
    @Qualifier(value = "xml")
    private Repository repository;

    @Override
    public void save(String message) {
        repository.save(message);
    }

}
