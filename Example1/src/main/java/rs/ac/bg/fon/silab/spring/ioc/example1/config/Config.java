package rs.ac.bg.fon.silab.spring.ioc.example1.config;

import org.springframework.context.annotation.Bean;
import rs.ac.bg.fon.silab.spring.ioc.example1.main.Main;
import rs.ac.bg.fon.silab.spring.ioc.example1.repository.Repository;
import rs.ac.bg.fon.silab.spring.ioc.example1.repository.impl.TxtRespository;
import rs.ac.bg.fon.silab.spring.ioc.example1.repository.impl.XmlRepository;
import rs.ac.bg.fon.silab.spring.ioc.example1.service.ServiceMessage;
import rs.ac.bg.fon.silab.spring.ioc.example1.service.impl.ServiceMessageImpl;

/**
 *
 * @author Dusan
 */
public class Config {

    @Bean(name = "txt")
    public Repository txtRepository() {
        return new TxtRespository();
    }

    @Bean(name = "xml")
    public Repository xmlRepository() {
        return new XmlRepository();
    }
    
     @Bean(name = "serviceMessage")
    public ServiceMessage serviceMessage(){
        return new ServiceMessageImpl(txtRepository());
    }
     
    @Bean
    public Main main(){
        return new Main(serviceMessage());
    }
}
