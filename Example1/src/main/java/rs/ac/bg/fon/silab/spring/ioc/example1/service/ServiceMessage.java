package rs.ac.bg.fon.silab.spring.ioc.example1.service;

/**
 *
 * @author Dusan
 */
public interface ServiceMessage {
    
    void save(String message);
    
}
