package rs.ac.bg.fon.silab.spring.ioc.example1.service.impl;

import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.silab.spring.ioc.example1.repository.Repository;
import rs.ac.bg.fon.silab.spring.ioc.example1.service.ServiceMessage;

/**
 *
 * @author Dusan
 */
@Service(value = "serviceMessage")
public class ServiceMessageImpl implements ServiceMessage {
    private  final Repository repository;
    
    @Inject
    public ServiceMessageImpl(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void save(String message) {
        repository.save(message);
    }

}
